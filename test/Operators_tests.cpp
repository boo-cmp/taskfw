#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <taskfw.h>

namespace {

class YieldTask : public taskfw::TaskWithResult<std::string>
{
public:
  void operator()() override
  {
    taskfw_task
    {
      m_result.push_back('a');
      taskfw_yield;
      m_result.push_back('b');
      taskfw_yield;
      m_result.push_back('c');
      taskfw_success_with_result(m_result);
    }
  }
private:
  std::string m_result;
};

class RestartTask : public taskfw::TaskWithResult<std::string>
{
public:
  explicit RestartTask(int _restart_count)
    : m_restart_count(_restart_count)
  {
  }

  void operator()() override
  {
    taskfw_task
    {
      m_result.push_back('a');
      taskfw_yield;
      m_result.push_back('b');
      if (m_restart_count--)
        taskfw_restart;
      m_result.push_back('c');

      taskfw_success_with_result(m_result);
    }
  }

private:
  std::string m_result;
  int m_restart_count;
};

class SetIntermediateTask : public taskfw::TaskWithResultAndIntermediate<std::string, std::string>
{
public:
  void operator()() override
  {
    taskfw_task
    {
      m_result.push_back('a');
      taskfw_set_intermediate(m_result);

      m_result.push_back('b');
      taskfw_set_intermediate(m_result);

      m_result.push_back('c');
      taskfw_set_intermediate(m_result);

      m_result.push_back('d');
      taskfw_yield;

      m_result.push_back('e');
      m_result.push_back('f');

      taskfw_success_with_result(m_result);
    }
  }
private:
  std::string m_result;
};

class WaitForTask : public taskfw::TaskWithResult<std::string>
{
public:
  void operator()() override
  {
    taskfw_task {
      m_subtask = taskfw::dispatch<YieldTask>();
      taskfw_wait_for_task(m_subtask);
      taskfw_wait(DoSomething());
      taskfw_success_with_result(m_subtask->GetResult());
    }
  }

private:
  YieldTask::DeferredResult m_subtask;

  bool DoSomething()
  {
    return m_something-- > 0;
  }

  int m_something = 10;
};

TEST(OperatorsTest, Yield)
{  
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID, dispatcher1);

  auto result = taskfw::dispatch<YieldTask>();

  int i = 0;
  while (!result->IsFinished()) {
    dispatcher1->Process();
    ++i;
  }

  EXPECT_EQ(i, 3);
  EXPECT_EQ(result->GetResult(), "abc");
}

}  // namespace

TEST(OperatorsTest, Restart)
{  
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID, dispatcher1);

  auto result = taskfw::dispatch<RestartTask>(3);

  while (!result->IsFinished())
    dispatcher1->Process();

  EXPECT_EQ(result->GetResult(), "ababababc");
}

TEST(OperatorsTest, SetIntermediate)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID, dispatcher1);

  auto result = taskfw::dispatch<SetIntermediateTask>();

  dispatcher1->Process();
  EXPECT_EQ(result->GetIntermediate(), "a");
  dispatcher1->Process();
  EXPECT_EQ(result->GetIntermediate(), "ab");
  dispatcher1->Process();
  EXPECT_EQ(result->GetIntermediate(), "abc");
  
  while (!result->IsFinished())
    dispatcher1->Process();

  EXPECT_EQ(result->GetResult(), "abcdef");
}

TEST(OperatorsTest, WaitFor)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID, dispatcher1);

  auto result = taskfw::dispatch<WaitForTask>();

  while (!result->IsFinished())
    dispatcher1->Process();

  EXPECT_EQ(result->GetResult(), "abc");
}
