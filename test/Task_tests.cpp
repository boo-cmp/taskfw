#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <taskfw.h>

#include <string>

const taskfw::DispatcherID disp_id_1 = taskfw::kMainDispatcherID;
const taskfw::DispatcherID disp_id_2 = 1;

namespace {

class SimpleTask : public taskfw::TaskWithResult<std::string, std::string> {
public:
  explicit SimpleTask(bool success)
    : m_success(success)
  {
  }

  void operator()() override
  {
    taskfw_task
    {
      m_result.append("1");
      m_result.append("2");
      taskfw_yield;
      m_result.append("3");
      taskfw_yield;

      if (!m_success)
        taskfw_fail_with_error("task failed");

      m_result.append("4");
      taskfw_yield;
      m_result.append("5");

      taskfw_success_with_result(m_result);
    }
  }

private:
  std::string m_result;
  bool m_success;
};

class SwitchableTask : public taskfw::TaskWithResult<std::string> {
public:
  void operator()() override
  {
    taskfw_task
    {
      m_result.append(std::to_string(taskfw_dispatcher)); // 0
      taskfw_switch_dispatcher(disp_id_2);

      m_result.append(std::to_string(taskfw_dispatcher)); // 1
      taskfw_switch_dispatcher(disp_id_1);

      m_result.append(std::to_string(taskfw_dispatcher)); // 0
      taskfw_switch_dispatcher(disp_id_1);

      m_result.append(std::to_string(taskfw_dispatcher)); // 0
      taskfw_switch_dispatcher(disp_id_2);

      m_result.append(std::to_string(taskfw_dispatcher)); // 1
      taskfw_switch_dispatcher(disp_id_2);

      taskfw_success_with_result(m_result);
    }
  }

private:
  std::string m_result;
};

}  // namespace

TEST(TaskTest, SimpleTaskResult)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();

  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_1, dispatcher1);

  auto result = taskfw::dispatch_to<SimpleTask, disp_id_1>(true);

  while (!result->IsFinished())
  {
    dispatcher1->Process();
  }

  EXPECT_FALSE(result->IsFailed());
  EXPECT_TRUE(result->IsSuccessful());
  EXPECT_EQ(result->GetResult(), "12345");
}

TEST(TaskTest, SimpleTaskError)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();

  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_1, dispatcher1);

  auto result = taskfw::dispatch_to<SimpleTask, disp_id_1>(false);

  while (!result->IsFinished())
  {
    dispatcher1->Process();
  }

  EXPECT_FALSE(result->IsSuccessful());
  EXPECT_TRUE(result->IsFailed());
  EXPECT_EQ(result->GetError(), "task failed");
}

TEST(TaskTest, SwitchDispatcher)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  auto dispatcher2 = std::make_shared<taskfw::ManualTaskDispatcher>();

  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_1, dispatcher1);
  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_2, dispatcher2);

  auto result = taskfw::dispatch_to<SwitchableTask, disp_id_1>();

  while (!result->IsFinished())
  {
    dispatcher2->Process();
    dispatcher1->Process();
  }

  EXPECT_FALSE(result->IsFailed());
  EXPECT_TRUE(result->IsSuccessful());
  EXPECT_EQ(result->GetResult(), "01001");
}

TEST(TaskTest, ResultCallback)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  auto dispatcher2 = std::make_shared<taskfw::ManualTaskDispatcher>();

  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_1, dispatcher1);
  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_2, dispatcher2);

  bool stop = false;

  taskfw::dispatch_to_on_callback<SwitchableTask, disp_id_1>(
    [&stop](SwitchableTask::DeferredResult result)
    {
      EXPECT_FALSE(result->IsFailed());
      EXPECT_TRUE(result->IsSuccessful());
      EXPECT_EQ(result->GetResult(), "01001");

      stop = true;
    });

  while (!stop)
  {
    dispatcher2->Process();
    dispatcher1->Process();
  }
}

TEST(TaskTest, ManySimpleTasksOnOneThread)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_1, dispatcher1);

  std::list<SimpleTask::DeferredResult> tasks;

  for (int i = 0; i < 10; ++i) {
    tasks.push_back(taskfw::dispatch_to<SimpleTask, disp_id_1>(true));
  }

  for (bool finish = false;!finish;) {
    dispatcher1->Process();

    finish = true;
    for (auto& result : tasks) {
      if (!result->IsFinished()) {
        finish = false;
        break;
      }
    }
  }

  for (auto& result : tasks) {
    EXPECT_FALSE(result->IsFailed());
    EXPECT_TRUE(result->IsSuccessful());
    EXPECT_EQ(result->GetResult(), "12345");
  }
}

TEST(TaskTest, ManyTasksOnTwoThreads)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  auto dispatcher2 = std::make_shared<taskfw::ThreadTaskDispatcher>(std::chrono::milliseconds(0));
  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_1, dispatcher1);
  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_2, dispatcher2);

  std::list<SimpleTask::DeferredResult> tasks;

  for (int i = 0; i < 10; ++i) {
    tasks.push_back(taskfw::dispatch_to<SimpleTask, disp_id_1>(true));
    tasks.push_back(taskfw::dispatch_to<SimpleTask, disp_id_2>(true));
  }

  for (bool finish = false;!finish;) {
    dispatcher1->Process();

    finish = true;
    for (auto& result : tasks) {
      if (!result->IsFinished()) {
        finish = false;
        break;
      }
    }
  }

  for (auto& result : tasks) {
    EXPECT_FALSE(result->IsFailed());
    EXPECT_TRUE(result->IsSuccessful());
    EXPECT_EQ(result->GetResult(), "12345");
  }
}

TEST(TaskTest, ManySwitchableTasks)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  auto dispatcher2 = std::make_shared<taskfw::ThreadTaskDispatcher>(std::chrono::milliseconds(0));
  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_1, dispatcher1);
  taskfw::Dispatcher::instance().AddTaskDispatcher(disp_id_2, dispatcher2);

  std::list<SwitchableTask::DeferredResult> tasks;

  for (int i = 0; i < 10; ++i) {
    tasks.push_back(taskfw::dispatch_to<SwitchableTask, disp_id_1>());
  }

  for (bool finish = false;!finish;) {
    dispatcher1->Process();

    finish = true;
    for (auto& result : tasks) {
      if (!result->IsFinished()) {
        finish = false;
        break;
      }
    }
  }

  for (auto& result : tasks) {
    EXPECT_FALSE(result->IsFailed());
    EXPECT_TRUE(result->IsSuccessful());
    EXPECT_EQ(result->GetResult(), "01001");
  }
}
