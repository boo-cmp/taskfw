#include "manual_task_dispatcher.h"

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <thread>

namespace taskfw
{

class ThreadTaskDispatcher : public ManualTaskDispatcher
{
public:
  explicit ThreadTaskDispatcher(std::chrono::steady_clock::duration _to_sleep)
    : to_sleep(_to_sleep)
    , shutdown(false)
  {
    working_thread = std::thread(&ThreadTaskDispatcher::Process, this);
  }

  ~ThreadTaskDispatcher() override
  {
    Stop();
  }

  void Dispatch(TaskPtr task) override
  {
    ManualTaskDispatcher::Dispatch(std::move(task));  // stores task in incoming queue.
    wait_for_incoming.notify_one();
  }

  void Stop() override
  {
    if (shutdown) return;

    shutdown = true;

    wait_for_incoming.notify_one();
    working_thread.join();
  }

private:

  void Process()
  {
    while (!shutdown)
    {
      if (inprogress_tasks.empty())
      {
        std::unique_lock<std::mutex> lock(incoming_lock);
        while (incoming_tasks.empty() && !shutdown)
        {
          wait_for_incoming.wait(lock);
        }
      }

      if (!shutdown)
      {
        ProcessIncoming();  // All incoming moved to in progress.
        ManualTaskDispatcher::ProcessInProgress();
        std::this_thread::sleep_for(to_sleep);
      }
    }
  }

  std::thread working_thread;
  const std::chrono::steady_clock::duration to_sleep;
  std::atomic<bool> shutdown;

  std::condition_variable wait_for_incoming;
};

}  // namespace taskfw
