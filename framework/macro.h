#pragma once

#define taskfw_task \
  switch (context.GetState()) \
  case ::taskfw::internal::TaskContext::kFinished: \
    if (true) \
    { \
    taskfw_finish_label: \
      context.Finish(); \
      return; \
    } else case ::taskfw::internal::TaskContext::kStart: \

// taskfw_finish_label also prevents using taskfw_task inside taskfw_task block!

#define taskfw_yield_n(n) \
  for (context.SetState(n);;) \
    if (true) return; \
  else case (n): \
    break;  // this breaks for loop.

#ifdef WIN32
#define taskfw_yield taskfw_yield_n(__COUNTER__ + 0xFFFF)
#else
#define taskfw_yield taskfw_yield_n(__LINE__ + 0xFFFF)
#endif


#define taskfw_restart \
  while (context.Restart(), true) return

#define taskfw_finish \
  goto taskfw_finish_label

#define taskfw_success \
  while (SetSuccessful<void>(), true) taskfw_finish

#define taskfw_fail \
  while (SetFailed<void>(), true) taskfw_finish

// Completes task with bool result.
#define taskfw_complete(success) \
  while (SetCompleted(success), true) taskfw_finish

#define taskfw_success_with_result(r) \
  while (SetResult(r), true) taskfw_finish

#define taskfw_fail_with_error(e) \
  while (SetError(e), true) taskfw_finish

#define taskfw_set_intermediate(ir) \
  do \
  { \
    SetIntermediate(ir); \
    taskfw_yield;\
  } while (false)

#define taskfw_set_intermediate_no_yield(ir) \
  SetIntermediate(ir)

#define taskfw_wait(expression) \
  while (!(expression)) taskfw_yield

#define taskfw_wait_for(expression) \
  taskfw_wait((expression));

#define taskfw_wait_for_finish(expr) \
  taskfw_wait((expr)->IsFinished())

#define taskfw_wait_for_task(task) \
  taskfw_wait_for_finish((task))

#define taskfw_dispatcher \
  context.GetCurrentDispatcherID()

#define taskfw_switch_dispatcher(dispatcher) \
  do { \
    if (taskfw_dispatcher != dispatcher) \
    { \
      context.ChangeDispatcherID(dispatcher);\
      taskfw_yield;\
    } \
  } while (false)
