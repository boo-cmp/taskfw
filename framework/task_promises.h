#pragma once

#include <mutex>

#include "task_context_and_state.h"

namespace taskfw {

template <typename ResultType, typename ErrorType = bool>
class ReturnResult : public virtual TaskState
{
public:

  bool IsSuccessful() const
  {
    return state == kSuccessfulResultState;
  }

  bool IsFailed() const
  {
    return state == kFailedResultState;
  }

  ResultType GetResult() const
  {
    return result;
  }

  ErrorType GetError() const
  {
    return error;
  }

protected:

  ReturnResult()
    : state(kUndefinedResultState)
  {
  }

  template <typename T>
  void SetSuccessful()
  {
    static_assert(std::is_same<ResultType, bool>::value, "taskfw_success valid only when ResultType == bool");
    SetResult(true);
  }

  template <typename T>
  void SetFailed()
  {
    static_assert(std::is_same<ErrorType, bool>::value, "taskfw_fail valid only when ErrorType == bool");
    SetError(true);
  }

  template <typename T>
  void SetCompleted(const bool success)
  {
    static_assert(std::is_same<ResultType, bool>::value, "taskfw_complete valid only when ResultType == bool");
    static_assert(std::is_same<ErrorType, bool>::value, "taskfw_complete valid only when ErrorType == bool");
    success ? SetResult(true) : SetError(true);
  }

  void SetResult(ResultType r)
  {
    result = std::move(r);
    state = kSuccessfulResultState;
  }

  void SetError(ErrorType e)
  {
    error = std::move(e);
    state = kFailedResultState;
  }

private:
  enum
  {
    kUndefinedResultState,
    kSuccessfulResultState,
    kFailedResultState,
  } state;

  ResultType  result;
  ErrorType   error;
};


template <typename ResultType, typename IntermediateType = float, typename ErrorType = bool>
class ReturnResultWithIntermediate : public ReturnResult<ResultType, ErrorType>
{
public:
  IntermediateType GetIntermediate() const
  {
    std::lock_guard<std::mutex> lock(mutex);
    return intermediate;
  }

protected:
  ReturnResultWithIntermediate()
  {
  }

  void SetIntermediate(IntermediateType i)
  {
    std::lock_guard<std::mutex> lock(mutex);
    intermediate = std::move(i);
  }

private:
  mutable std::mutex mutex;
  IntermediateType intermediate;
};



}
